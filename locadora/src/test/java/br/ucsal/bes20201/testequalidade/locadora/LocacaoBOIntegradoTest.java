package br.ucsal.bes20201.testequalidade.locadora;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import br.ucsal.bes20201.testequalidade.locadora.business.LocacaoBO;
import br.ucsal.bes20201.testequalidade.locadora.dominio.Cliente;
import br.ucsal.bes20201.testequalidade.locadora.dominio.Modelo;
import br.ucsal.bes20201.testequalidade.locadora.dominio.Veiculo;
import br.ucsal.bes20201.testequalidade.builder.VeiculoBuilder;

public class LocacaoBOIntegradoTest {

	/**
	 * Testar o cálculo do valor de locação por 3 dias para 2 veículos com 1 ano de
	 * fabricaçao e 3 veículos com 8 anos de fabricação.
	 */
	@Test
	public void testarCalculoValorTotalLocacao5Veiculos3Dias() {
		Modelo modelo = new Modelo("Honda");
		Cliente cliente = new Cliente("55", "Cliente", "777");
		LocacaoBO locacao = new LocacaoBO();
		
		VeiculoBuilder veiculoBuilder = VeiculoBuilder.umVeiculo()
				.comModelo(modelo);
		
		Veiculo veic1 = veiculoBuilder.build();
		Veiculo veic2 = veiculoBuilder.build();
		
		Veiculo veic3 = veiculoBuilder.mas().comAno(2014).build();
		Veiculo veic4 = veiculoBuilder.mas().comAno(2014).build();
		Veiculo veic5 = veiculoBuilder.mas().comAno(2014).build();
		
		List<Veiculo> veiculos = new ArrayList<Veiculo>();
		veiculos.add(veic1);
		veiculos.add(veic2);
		veiculos.add(veic3);
		veiculos.add(veic4);
		veiculos.add(veic5);
		
		Double valorEsperado = 2800d; 
		
		Assertions.assertEquals(valorEsperado, locacao.calcularValorTotalLocacao(veiculos, 3));
		
	}

}