package br.ucsal.bes20201.testequalidade.builder;

import br.ucsal.bes20201.testequalidade.locadora.dominio.Modelo;
import br.ucsal.bes20201.testequalidade.locadora.dominio.Veiculo;
import br.ucsal.bes20201.testequalidade.locadora.dominio.enums.SituacaoVeiculoEnum;

public class VeiculoBuilder {

	private static final String PLACA_DEFAULT = "AAA-5522";
	private static final Integer ANO_DEFAULT = 2017;
	private static final Modelo MODELO_DEFAULT = null;
	private static final Double VALOR_DEFAULT = 200.00;
	private static final SituacaoVeiculoEnum SITUACAO_DEFAULT = SituacaoVeiculoEnum.DISPONIVEL;
	
	private String placa = PLACA_DEFAULT;
	private Integer ano = ANO_DEFAULT;
	private Modelo modelo = MODELO_DEFAULT;
	private Double valorDiaria = VALOR_DEFAULT;
	private SituacaoVeiculoEnum situacao = SITUACAO_DEFAULT;
	
	private VeiculoBuilder() {
	}
	
	public static VeiculoBuilder umVeiculo() {
		return new VeiculoBuilder();
	}
	
	public VeiculoBuilder comPlaca(String placa) {
		this.placa = placa;
		return this;
	}
	
	public VeiculoBuilder comAno(Integer ano) {
		this.ano = ano;
		return this;
	}
	
	public VeiculoBuilder comModelo(Modelo modelo) {
		this.modelo = modelo;
		return this;
	}
	
	public VeiculoBuilder comValorDiaria(Double valorDiaria) {
		this.valorDiaria = valorDiaria;
		return this;
	}
	
	public VeiculoBuilder comSituacao(SituacaoVeiculoEnum situacao) {
		this.situacao = situacao;
		return this;
	}
	
	public VeiculoBuilder mas() {
		return umVeiculo().comPlaca(placa).comAno(ano).comModelo(modelo).comValorDiaria(valorDiaria).comSituacao(situacao);
	}
	
	public Veiculo build() {
		Veiculo veiculo = new Veiculo();
		veiculo.setPlaca(placa);
		veiculo.setAno(ano);
		veiculo.setModelo(modelo);
		veiculo.setValorDiaria(valorDiaria);
		veiculo.setSituacao(situacao);
		return veiculo;
	}
	
}
